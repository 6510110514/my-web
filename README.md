# CREATE YOUR PORTFOLIO 
This project is a web page for creating your portfolio, using Flask as the main operator.
It is part of the course BASIC ARTIFICIAL INTELLIGENCE SYSTEMS DEVELOPMENT MODULE


## Installation

Clone the repo: https://gitlab.com/6510110514/my-web.git

Create an environment


python3 -m venv venv
soure /venv/bin/activate # window ./venv/Scripts/activate.bat
pip install poetry


Install the library
" pip install -r requirements.txt "